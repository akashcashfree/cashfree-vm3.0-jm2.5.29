<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * * http://gocashfree.com
 */

defined('_JEXEC') or die('Restricted access');

class CashFreeHelperCustomerData {

	private $_selected_method = '';
	
	private $_errormessage = array();
	private $_customer_name = '';
	private $_customer_email = '';
	private $_customer_phone = '';
	private $_payer_id = '';



	public function load() {

		//$this->clear();
		if (!class_exists('vmCrypt')) {
			require(VMPATH_ADMIN . DS . 'helpers' . DS . 'vmcrypt.php');
		}
		$session = JFactory::getSession();
		$sessionData = $session->get('cashfree', 0, 'vm');

		if (!empty($sessionData)) {
			$data =   (object)json_decode($sessionData, true);
			$this->_selected_method = $data->selected_method;
			// card information
		
			$this->_payer_id = $data->payer_id;
			$this->_customer_name = $data->customer_name;
			$this->_customer_email = $data->customer_email;
			$this->_customer_phone = $data->customer_phone;

			$this->save();
			return $data;
		}
	}

	public function loadPost() {
		if (!class_exists('vmCrypt')) {
			require(VMPATH_ADMIN . DS . 'helpers' . DS . 'vmcrypt.php');
		}
		// card information
		$virtuemart_paymentmethod_id = vRequest::getVar('virtuemart_paymentmethod_id', 0);
		//if ($virtuemart_paymentmethod_id) {
		//	print_trace();
		//$this->clear();
		//}

		$this->_selected_method = $virtuemart_paymentmethod_id;
		
		$this->save();
	}

	public function save() {
		if (!class_exists('vmCrypt')) {
			require(VMPATH_ADMIN . DS . 'helpers' . DS . 'vmcrypt.php');
		}
		$session = JFactory::getSession();
		$sessionData = new stdClass();
		$sessionData->selected_method = $this->_selected_method;
	
		//PayPal Express
		$sessionData->payer_id = $this->_payer_id;
		$sessionData->customer_name = $this->_customer_name;
		$sessionData->customer_email = $this->_payer_email;
        $sessionData->customer_phone = $this->_customer_phone;


//		$sessionData->txn_id = $this->_txn_id;
//		$sessionData->txn_type = $this->_txn_type;
//		$sessionData->payment_status = $this->_payment_status;
//		$sessionData->pending_reason = $this->_pending_reason;

		$session->set('cashfree', json_encode($sessionData), 'vm');
	}

	public function reset() {
		$this->_selected_method = '';
		
		$this->_payer_id = '';
		$this->_customer_name = '';
		$this->_customer_phone = '';
		$this->_customer_email = '';


		$this->save();
	}

	public function clear() {
		$session = JFactory::getSession();
		$session->clear('cashfree', 'vm');
	}

	public function getVar($var) {
		$this->load();
		return $this->{'_' . $var};
	}

	public function setVar($var, $val) {
		$this->{'_' . $var} = $val;
	}

}
