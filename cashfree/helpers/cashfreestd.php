<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * * http://gocashfree.com
 */


defined('_JEXEC') or die('Restricted access');

//https://cms.paypal.com/mx/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_html_Appx_websitestandard_htmlvariables

class CashFreeHelperCashFreeStd extends CashFreeHelperCashFree {

	var $app_id = '';
	var $secret_key = '';
	var $post_variables = '';


	function __construct($method, $cashfreePlugin) {
		parent::__construct($method, $cashfreePlugin);
		//Set the credentials
		if ($this->_method->sandbox) {
			$this->app_id = $this->_method->sandbox_cashfree_app_id;
			$this->secret_key = $this->_method->sandbox_cashfree_secret_key;
		} else {
			$this->app_id= $this->_method->cashfree_app_id;
			$this->secret_key = $this->_method->cashfree_secret_key;
		}
		if (empty($this->app_id) || empty($this->secret_key)) {
			$sandbox = "";
			if ($this->_method->sandbox) {
				$sandbox = 'SANDBOX_';
			}
			$text = vmText::sprintf('CashFree auth parameters are missing', vmText::_('Fill auth parameters'), $this->_method->payment_name, $this->_method->virtuemart_paymentmethod_id);
			vmError($text, $text);
			return FALSE;
		}
	}

	public function processCashFreeResponse($cashfree_response) {

	}

	public function ManageCheckout() {

		$post_variables = $this->post_variables;
		$order_number_text=$this->getItemName(vmText::_('COM_VIRTUEMART_ORDER_NUMBER'));

	  //  $post_variables['orderNote'] = $order_number_text . ': ' . $this->order['details']['BT']->order_number;
		$post_variables['orderAmount'] = $this->total;
        $post_variables['appId'] = $this->app_id;
        $post_variables['secretKey'] = $this->secret_key;
        $post_variables['mode'] = "ONLINE";

        $url = $this->_getCashFreeUrl();
       // var_dump($url);
        $apiResponse = $this->sendRequest($post_variables, $url);

		$html = '';
		//var_dump($apiResponse);
		if ($apiResponse->{'status'} == "OK") {
			vmJsApi::addJScript('vm.paymentFormAutoSubmit', '
  		    	jQuery(document).ready(function($){
   			    	jQuery("body").addClass("vmLoading");
    			   jQuery("#vmPaymentForm").submit();
 			    })
       		');
 
            if ($this->_method->debug) {
			  $html = '<form action="'.$apiResponse->{"paymentLink"}.'" method="GET">';
			} else { 
			  $html = '<form action="'.$apiResponse->{"paymentLink"}.'" method="GET" id="vmPaymentForm">';
			}
			$html .= '<input type="submit" value="Redirecting you to CashFree.." size="40"/>';
			$html .= '</form>';
		} else {
			$html .= 'Something wrong has happened with payment. Please try again.';

		}
		return $html;
	}

	function setAPIPostVariables() {

		$address = ((isset($this->order['details']['ST'])) ? $this->order['details']['ST'] : $this->order['details']['BT']);
 

		$post_variables = Array();
		$post_variables['redirect_cmd'] = $payment_type;
		$post_variables['orderId'] = $this->order['details']['BT']->order_number;
		$post_variables['customerName'] = $address->first_name." ".$address->last_name;
		$post_variables['address1'] = $address->address_1;
		$post_variables['address2'] = isset($address->address_2) ? $address->address_2 : '';
		$post_variables['zip'] = $address->zip;
		$post_variables['city'] = $address->city;
		$post_variables['customerEmail'] = $this->order['details']['BT']->email;
		$post_variables['customerPhone'] = $this->returnMobile($address->phone_1, $address->phone_2);

		$post_variables['returnUrl'] = JURI::root() . 'index.php?option=com_virtuemart&view=vmplg&task=pluginresponsereceived&on=' . $this->order['details']['BT']->order_number . '&pm=' . $this->order['details']['BT']->virtuemart_paymentmethod_id . '&Itemid=' . vRequest::getInt('Itemid') . '&lang=' . vRequest::getCmd('lang', '');
		$post_variables['notifyUrl'] = JURI::root() . 'index.php?option=com_virtuemart&view=vmplg&task=notify&tmpl=component' . '&lang=' . vRequest::getCmd('lang', '');

        $this->post_variables = $post_variables;
		return $post_variables;
	}

	function returnMobile($phone_1, $phone_2) {
       $phone = (strlen($phone_1) > 0) ?  $phone_1 : $phone_2;
       return $phone;
	}

	function getExtraPluginInfo() {
		return;
	}

	function getOrderBEFields() {
		$showOrderBEFields = array(
			'PAYMENT_DATE' => 'payment_date',
			'PAYMENT_STATUS' => 'payment_status',
			'REFERENCE_ID' => 'reference_id',
			'PAYMENT_MODE' => 'payment_mode',
			'MESSAGE' => 'message'
		);
		return $showOrderBEFields;
	}

	function onShowOrderBEPaymentByFields($payment) {
		$prefix = "cashfree_response_";
		$html = "";
		$showOrderBEFields = $this->getOrderBEFields();
		foreach ($showOrderBEFields as $key => $showOrderBEField) {
			$field = $prefix . $showOrderBEField;
			// only displays if there is a value or the value is different from 0.00 and the value
			if ($payment->$field) {
				$html .= $this->CashFreePlugin->getHtmlRowBE($prefix . $key, $payment->$field);
			}
		}


		return $html;
	}
}
