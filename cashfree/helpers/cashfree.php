<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * * http://gocashfree.com
 */


defined('_JEXEC') or die('Restricted access');

class CashFreeHelperCashFree {

	var $_method;
	var $cart;
	var $order;
	var $vendor;
	var $customerData;
	var $context;
	var $total;
	var $post_variables;
	var $post_string;
	var $requestData;
	var $response;
	var $currency_code_3;
	var $currency_display;
	var $cashfreePlugin;


	const FRAUD_FAILURE_ERROR_CODE = 10486;
	const FMF_PENDED_ERROR_CODE = 11610;
	const FMF_DENIED_ERROR_CODE = 11611;
	const BNCODE = "VirtueMart_Cart_PPA";


	function __construct ($method, $cashfreePlugin) {
		$session = JFactory::getSession();
		$this->context = $session->getId();
		$this->_method = $method;
		$this->CashFreePlugin = $cashfreePlugin;
		//Set the vendor
		$vendorModel = VmModel::getModel('Vendor');
		$vendorModel->setId($this->_method->virtuemart_vendor_id);
		$vendor = $vendorModel->getVendor();
		$vendorModel->addImages($vendor, 1);
		$this->vendor = $vendor;

		$this->getCashFreePaymentCurrency();
	}

	function getCashFreePaymentCurrency ($getCurrency = FALSE) {

		vmPSPlugin::getPaymentCurrency($this->_method);
		$this->currency_code_3 = shopFunctions::getCurrencyByID($this->_method->payment_currency, 'currency_code_3');

	}

	public function getContext () {
		return $this->context;
	}

	public function setCart ($cart) {
		$this->cart = $cart;
		if (!isset($this->cart->cartPrices) or empty($this->cart->cartPrices)) {
			$this->cart->prepareCartData();
		}
	}

	public function setOrder ($order) {
		$this->order = $order;
	}

	public function setCustomerData ($customerData) {
		$this->customerData = $customerData;
	}

	public function loadCustomerData () {
		$this->customerData = new CashFreeHelperCustomerData();
		$this->customerData->load();
		$this->customerData->loadPost();
	}

	/*
	 *  removing  all but alphanumeric characters & spaces.
	 */
	function getItemName ($name) {
		$name= substr(strip_tags($name), 0, 127);
		$name = preg_replace('/[^a-zA-Z0-9\s]/', '', $name);
		return $name;
	}

	function getProductAmount ($productPricesUnformatted) {
		if ($productPricesUnformatted['salesPriceWithDiscount']) {
			return vmPSPlugin::getAmountValueInCurrency($productPricesUnformatted['salesPriceWithDiscount'], $this->_method->payment_currency);
		} else {
			return vmPSPlugin::getAmountValueInCurrency($productPricesUnformatted['salesPrice'], $this->_method->payment_currency);
		}
	}

	function getProductAmountWithoutTax ($productPricesUnformatted) {
		if ($productPricesUnformatted['discountedPriceWithoutTax']) {
			return vmPSPlugin::getAmountValueInCurrency($productPricesUnformatted['discountedPriceWithoutTax'], $this->_method->payment_currency);
		} else {
			return vmPSPlugin::getAmountValueInCurrency($productPricesUnformatted['priceBeforeTax'], $this->_method->payment_currency);
		}
	}

	function getProductTaxAmount ($productPricesUnformatted) {
		if ($productPricesUnformatted['subtotal_tax_amount']) {
			return vmPSPlugin::getAmountValueInCurrency($productPricesUnformatted['subtotal_tax_amount'], $this->_method->payment_currency);
		}
	}



	public function setTotal ($total) {
		if (!class_exists('CurrencyDisplay')) {
			require(VMPATH_ADMIN . DS  .'helpers'.DS.'currencydisplay.php');
		}
		$this->total = vmPSPlugin::getAmountValueInCurrency($total, $this->_method->payment_currency);

		$cd = CurrencyDisplay::getInstance($this->cart->pricesCurrency);
	}

	public function getTotal () {
		return $this->total;
	}

	public function getResponse () {
		return $this->response;
	}

	public function getRequest () {
		return $this->requestData;
	}

	protected function sendRequest ($post_data, $post_url) {
		$retryCodes = array('401', '403', '404',);

		$this->post_data = $post_data;

		$post_string = $this->ToUri($post_data);
		$curl_request = curl_init($post_url);
		curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($curl_request, CURLOPT_HEADER, 0);
		curl_setopt($curl_request, CURLOPT_TIMEOUT, $this->_timeout);
		curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);


		curl_setopt($curl_request, CURLOPT_POST, count($post_data));
		
		$response = curl_exec($curl_request);

		if ($curl_error = curl_error($curl_request)) {
			$this->debugLog($curl_error, '----CURL ERROR----', 'error');
		}
		/*
				$httpStatus = curl_getinfo($curl_request, CURLINFO_HTTP_CODE);
				$retries = 0;
				if(in_array($httpStatus, $retryCodes) && isset($this->retry)) {
					$this->debugLog("Got $httpStatus response from server. Retrying");

					do 	{
						$result = curl_exec(debugLog);
						$httpStatus = curl_getinfo(debugLog, CURLINFO_HTTP_CODE);

					} while (in_array($httpStatus, self::$retryCodes) && ++$retries < $this->retry );


				}
				*/

	
		curl_close($curl_request);

		
		// var_dump($response);
		$responseObj = json_decode($response);

		if ($responseObj->{"status"} == 'FAILED') {
			$level = 'warning';
		} else {
			$level = 'debug';
		}

//		$this->debugLog($post_data, 'CashFree ' . $post_data['METHOD'] . ' Request variables:', $level);
//		$this->debugLog($this->response, 'CashFree response:', $level);

		return $responseObj;

	}

	
	protected function _getCashFreeUrl ($protocol = 'https://') {
		$url = ($this->_method->sandbox) ? $protocol . 'test.gocashfree.com' : $protocol . 'api.gocashfree.com';
	    $url .= '/api/v1/order/create';
		return $url;
	}
	
	protected function truncate ($string, $length) {
		if (!class_exists('shopFunctionsF')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		}
		return ShopFunctionsF::vmSubstr($string, 0, $length);
	}

	protected function _getFormattedDate ($month, $year) {

		return sprintf('%02d%04d', $month, $year);
	}

	public function validate ($enqueueMessage = true) {
		return true;
	}

	public function validatecheckout ($enqueueMessage = true) {
		return true;
	}

	function ToUri ($post_variables) {
		$poststring = '';
		foreach ($post_variables AS $key => $val) {
			$poststring .= urlencode($key) . "=" . urlencode($val) . "&";
		}
		$poststring = rtrim($poststring, "& ");
		return $poststring;
	}


	public function getLogoImage () {
		if ($this->_method->logoimg) {
			return JURI::base() . '/images/stories/virtuemart/payment/' . $this->_method->logoimg;
		} else {
			return JURI::base() . $this->vendor->images[0]->file_url;
		}

	}




	


	/*********************/
	/* Log and Reporting */
	/*********************/
	public function debug ($subject, $title = '', $echo = true) {

		$debug = '<div style="display:block; margin-bottom:5px; border:1px solid red; padding:5px; text-align:left; font-size:10px;white-space:nowrap; overflow:scroll;">';
		$debug .= ($title) ? '<br /><strong>' . $title . ':</strong><br />' : '';
		//$debug .= '<pre>';
		$debug .= str_replace("=>", "&#8658;", str_replace("Array", "<font color=\"red\"><b>Array</b></font>", nl2br(str_replace(" ", " &nbsp; ", print_r($subject, true)))));
		//$debug .= '</pre>';
		$debug .= '</div>';
		if ($echo) {
			echo $debug;
		} else {
			return $debug;
		}
	}

	function highlight ($string) {
		return '<span style="color:red;font-weight:bold">' . $string . '</span>';
	}

	public function debugLog ($message, $title = '', $type = 'message', $echo = false, $doVmDebug = false) {
		$masked_fields = array('ACCT', 'CVV2', 'signature', 'SIGNATURE', 'api_password', 'PWD');
		//Nerver log the full credit card number nor the CVV code.
		if (is_array($message)) {
			foreach ($masked_fields as $masked_field) {
				if (array_key_exists($masked_field, $message)) {
					$message[$masked_field] = '**MASKED**';
				}
			}

		}

		if ($this->_method->debug) {
			$this->debug($message, $title, true);
		}

		if ($echo) {
			echo $message . '<br/>';
		}


		$this->CashFreePlugin->debugLog($message, $title, $type, $doVmDebug);
	}


}
