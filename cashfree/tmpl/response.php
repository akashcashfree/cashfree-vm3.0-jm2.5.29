<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * * http://gocashfree.com
 */

defined('_JEXEC') or die();

$cashfree_response = $viewData["cashfree_response"];
$payment_name = $viewData["payment_name"];
$payment = $viewData["payment"];
$order = $viewData["order"];
$referenceId = $cashfree_response["referenceId"];
$txStatus = $cashfree_response["txStatus"];
$currency_symbol = shopFunctions::getCurrencyByID($payment->payment_currency, 'currency_symbol');
//$totalAmount 

?>
<br />
<?php if ($txStatus == "SUCCESS") { ?>
   <h3 style="color: green;">Your payment was successful</h3><br/>
<?php } ?>
<table cellpadding="10" cellspacing="10">
	<tr>
    	<td style="padding: 10px;"><h4>Order ID</h4></td>
        <td style="padding: 10px;"><?php echo $order['details']['BT']->order_number; ?></td>
    </tr>
    <tr>
        <td style="padding: 10px;"><h4>Transaction Status</h4></td>
        <td style="padding: 10px;"><?php echo $txStatus; ?></td>
    </tr>
	<tr>
		<td style="padding: 10px;"><h4>Order Amount</h4></td>
        <td style="padding: 10px;"><?php echo $currency_symbol;?> <?php echo $payment->payment_order_total; ?></td>
    </tr>

	<tr >
    	<td style="padding: 10px;"><h4>CashFree Reference Id</h4></td>
        <td style="padding: 10px;"><?php echo $referenceId;  ?></td>
    </tr>

</table>

	<br />
	<a class="vm-button-correct" href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=orders&layout=details&order_number='.$viewData["order"]['details']['BT']->order_number.'&order_pass='.$viewData["order"]['details']['BT']->order_pass, false)?>"><?php echo vmText::_('COM_VIRTUEMART_ORDER_VIEW_ORDER'); ?></a>
