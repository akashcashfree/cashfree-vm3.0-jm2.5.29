<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * http://gocashfree.com
 */
defined('JPATH_BASE') or die();

jimport('joomla.form.formfield');
class JFormFieldGetCashFree extends JFormField {

	/**
	 * Element name
	 *
	 * @access    protected
	 * @var        string
	 */
	var $type = 'getCashFree';

	protected function getInput() {

		JHtml::_('behavior.colorpicker');
                $url = "http://gocashfree.com";
		$logo = '<img src="https://s3-ap-southeast-1.amazonaws.com/cfmerchantlogo/cf_merchant_logo.png"/>';
		$html = '<p><a target="_blank" href="' . $url . '"  >' . $logo . '</a></p>';
		return $html;
	}

}
