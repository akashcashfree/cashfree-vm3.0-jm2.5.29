<?php
/**
 *
 * CashFree payment plugin
 *
 * @author Akash Sinha
 * * http://gocashfree.com
 */

defined('_JEXEC') or die('Restricted access');
if (!class_exists('vmPSPlugin')) {
	require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php');
}

if (!class_exists('CashFreeHelperCashFree')) {
        require(VMPATH_ROOT .DS.'plugins'.DS.'vmpayment'.DS.'cashfree'.DS.'cashfree'.DS.'helpers'.DS.'cashfree.php');
}
if (!class_exists('CashFreeHelperCashFreeStd')) {
        require(VMPATH_ROOT . DS.'plugins'.DS.'vmpayment'.DS.'cashfree'.DS.'cashfree'.DS.'helpers'.DS.'cashfreestd.php');
}
if (!class_exists('CashFreeHelperCustomerData')) {
	require(VMPATH_ROOT .DS.'plugins'.DS.'vmpayment'.DS.'cashfree'.DS.'cashfree'.DS.'helpers'.DS.'customerdata.php');
}

class plgVmPaymentCashFree extends vmPSPlugin {

	// instance of class
	private $customerData;
	private $_autobilling_max_amount = '';
	private $_errormessage = array();

	function __construct(& $subject, $config) {

		//if (self::$_this)
		//   return self::$_this;
		parent::__construct($subject, $config);

		$this->customerData = new CashFreeHelperCustomerData();
		$this->_loggable = TRUE;
		$this->tableFields = array_keys($this->getTableSQLFields());
		$this->_tablepkey = 'id'; //virtuemart_cashfree_id';
		$this->_tableId = 'id'; //'virtuemart_cashfree_id';
		$varsToPush = array(
			'cashfree_app_id' => array('', 'char'),
			'cashfree_secret_key' => array('', 'char'),
			
			'sandbox' => array(0, 'int'),
			'sandbox_cashfree_app_id' => array('', 'char'),
			'sandbox_cashfree_secret_key' => array('', 'char'),
			'debug' => array(0, 'int'),
			'order_change_trigger' => array(0, 'int'),
	
			'status_success' => array('', 'char'),
			'status_canceled' => array('', 'char'),
			'status_pending' => array('', 'char'),
			'status_refunded' => array('', 'char'),
			'status_partial_refunded' => array('', 'char'),
		);

		$this->setConfigParameterable($this->_configTableFieldName, $varsToPush);
		//self::$_this = $this;
	}

	public function getVmPluginCreateTableSQL() {
		return $this->createTableSQL('CashFree Table');
	}

	function getTableSQLFields() {

		$SQLfields = array(
			'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT',
			'virtuemart_order_id' => 'int(1) UNSIGNED',
			'order_number' => 'char(64)',
			'cashfree_custom' => 'varchar(120)',
			'virtuemart_paymentmethod_id' => 'mediumint(1) UNSIGNED',
			'payment_name' => 'varchar(5000)',
			'payment_order_total' => 'decimal(15,5) NOT NULL',
			'payment_currency' => 'char(3)',
			'email_currency' =>  'char(3)',
			'cost_per_transaction' => 'decimal(10, 2)',
			'cost_min_transaction' => 'decimal(10, 2)',
			'cost_percent_total' => 'decimal(10, 2)',
			'tax_id' => 'smallint(1)',
			'isProcessed' => 'smallint(1)',
			'customer_email' => 'varchar(256)',
			'customer_name' => 'varchar(256)',
			'customer_phone' => 'varchar(15)',
			'cashfree_response_payment_date' => 'varchar(28)',
			'cashfree_response_payment_status' => 'varchar(50)',
			'cashfree_response_payment_reference_id' => 'varchar(50)',
			'cashfree_response_payment_mode' => 'varchar(50)',
			'cashfree_response_message' => 'varchar(50)',
			'cashfree_fullresponse' => 'text',
		);
		return $SQLfields;
	}

	/**
	 * @param $product
	 * @param $productDisplay
	 * @return bool
	 */
	function plgVmOnProductDisplayPayment($product, &$productDisplay) {
		return;
		$vendorId = 1;
		if ($this->getPluginMethods($vendorId) === 0) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * @param VirtuemartViewUser $user
	 * @param                    $html
	 * @param bool               $from_cart
	 * @return bool|null
	 */
	function plgVmDisplayLogin(VirtuemartViewUser $user, &$html, $from_cart = FALSE) {

		// only to display it in the cart, not in list orders view
		if (!$from_cart) {
			return NULL;
		}

		$vendorId = 1;
		if (!class_exists('VirtueMartCart')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'cart.php');
		}

		$cart = VirtueMartCart::getCart();
		if ($this->getPluginMethods($cart->vendorId) === 0) {
			return FALSE;
		}

		if (!($selectedMethod = $this->getVmPluginMethod($cart->virtuemart_paymentmethod_id))) {
			return FALSE;
		}

		return;

	}

	/**
	 * @param $cart
	 * @param $payment_advertise
	 * @return bool|null
	 */
	function plgVmOnCheckoutAdvertise($cart, &$payment_advertise) {

		if ($this->getPluginMethods($cart->vendorId) === 0) {
			return FALSE;
		}
		if (!($selectedMethod = $this->getVmPluginMethod($cart->virtuemart_paymentmethod_id))) {
			return NULL;
		}
		if (isset($cart->cartPrices['salesPrice']) && $cart->cartPrices['salesPrice'] <= 0.0) {
			return NULL;
		}
		
		return;
	}

    /**
	 * Check if the payment conditions are fulfilled for this payment method
	 *
	 *
	 * @param $cart_prices: cart prices
	 * @param $payment
	 * @return true: if the conditions are fulfilled, false otherwise
	 *
	 */
	protected function checkConditions ($cart, $method, $cart_prices) {

		$this->convert_condition_amount($method);
		$amount = $this->getCartAmount($cart_prices);
		$address = (($cart->ST == 0) ? $cart->BT : $cart->ST);


		//vmdebug('standard checkConditions',  $amount, $cart_prices['salesPrice'],  $cart_prices['salesPriceCoupon']);
		$amount_cond = ($amount >= $method->min_amount AND $amount <= $method->max_amount
			OR
			($method->min_amount <= $amount AND ($method->max_amount == 0)));
		if (!$amount_cond) {
			return FALSE;
		}
		$countries = array();
		if (!empty($method->countries)) {
			if (!is_array ($method->countries)) {
				$countries[0] = $method->countries;
			} else {
				$countries = $method->countries;
			}
		}

		// probably did not gave his BT:ST address
		if (!is_array ($address)) {
			$address = array();
			$address['virtuemart_country_id'] = 0;
		}

		if (!isset($address['virtuemart_country_id'])) {
			$address['virtuemart_country_id'] = 0;
		}
		if (count ($countries) == 0 || in_array ($address['virtuemart_country_id'], $countries) ) {
			return TRUE;
		}

		return FALSE;
	}


	/*
* We must reimplement this triggers for joomla 1.7
*/

	/**
	 *
	 * @param $cart
	 * @param $order
	 * @return bool|null|void
	 */
	function plgVmConfirmedOrder($cart, $order) {

		if (!($this->_currentMethod = $this->getVmPluginMethod($order['details']['BT']->virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return FALSE;
		}

		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}
		if (!class_exists('VirtueMartModelCurrency')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'currency.php');
		}
		$html='';
		//$this->getPaymentCurrency($this->_currentMethod);
		$this->_currentMethod->payment_currency=$order['details']['BT']->user_currency_id;

        $this->getPaymentCurrency($this->_currentMethod);
        $email_currency = $this->getEmailCurrency($this->_currentMethod);
        $totalInPaymentCurrency = vmPSPlugin::getAmountInCurrency($order['details']['BT']->order_total, $this->_currentMethod->payment_currency);


		$payment_name = $this->renderPluginName($this->_currentMethod, $order);

		$CashFreeInterface = $this->_loadCashFreeInterface();
		$CashFreeInterface->debugLog('order number: ' . $order['details']['BT']->order_number, 'plgVmConfirmedOrder', 'message');
		$CashFreeInterface->setCart($cart);
		$CashFreeInterface->setOrder($order);
		$CashFreeInterface->setTotal($order['details']['BT']->order_total);
		$CashFreeInterface->loadCustomerData();

	    $post_variables = $CashFreeInterface->setAPIPostVariables();

		// Prepare data that should be stored in the database
		$dbValues['order_number'] = $order['details']['BT']->order_number;
		$dbValues['virtuemart_order_id'] = $order['details']['BT']->virtuemart_order_id;
		$dbValues['payment_name'] = $payment_name;
		$dbValues['virtuemart_paymentmethod_id'] = $cart->virtuemart_paymentmethod_id;
		$dbValues['payment_currency'] = $order['details']['BT']->user_currency_id;
		$dbValues['email_currency'] = $email_currency;
		$dbValues['payment_order_total'] = $CashFreeInterface->getTotal();
		$dbValues['tax_id'] = $this->_currentMethod->tax_id;
		$dbValues['customer_name'] = $post_variables["customerName"];
		$dbValues['customer_email'] = $post_variables["customerEmail"];
		$dbValues['customer_phone'] = $post_variables["customerPhone"];
		$dbValues['cashfree_custom'] = $CashFreeInterface->getContext();
		$dbValues['cashfree_response_payment_status'] = "INCOMPLETE";
		$dbValues["isProcessed"] = 0;
        $dbValues['cost_per_transaction'] = $this->_currentMethod->cost_per_transaction;
        $dbValues['cost_min_transaction'] = $this->currentMethod->cost_min_transaction;

		$this->storePSPluginInternalData($dbValues);
		VmConfig::loadJLang('com_virtuemart_orders', TRUE);


			$html = $CashFreeInterface->ManageCheckout();
			// 	2 = don't delete the cart, don't send email and don't redirect
			$cart->_confirmDone = FALSE;
			$cart->_dataValidated = FALSE;
			$cart->setCartIntoSession();

			vRequest::setVar('html', $html);
	}

	/**
	 * @param null $msg
	 */
	function redirectToCart ($msg = NULL) {
		if (!$msg) {
			$msg = vmText::_('Payment Failed. Try Again.');
		}
		$this->customerData->clear();
		$app = JFactory::getApplication();
		$app->redirect(JRoute::_('index.php?option=com_virtuemart&view=cart&Itemid=' . vRequest::getInt('Itemid'), false), $msg);
	}

	/**
	 * @param $virtuemart_paymentmethod_id
	 * @param $paymentCurrencyId
	 * @return bool|null
	 */
	function plgVmgetPaymentCurrency($virtuemart_paymentmethod_id, &$paymentCurrencyId) {

		if (!($this->_currentMethod = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return NULL; // Another/ method was selected, do nothing
		}
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return FALSE;
		}
		$this->getPaymentCurrency($this->_currentMethod);
		$paymentCurrencyId = $this->_currentMethod->payment_currency;
	}

	function plgVmgetEmailCurrency($virtuemart_paymentmethod_id, $virtuemart_order_id, &$emailCurrencyId) {

		if (!($this->_currentMethod = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return FALSE;
		}
		if (!($payments = $this->_getCashFreeInternalData($virtuemart_order_id))) {
			// JError::raiseWarning(500, $db->getErrorMsg());
			return '';
		}
		if (empty($payments[0]->email_currency)) {
			$vendorId = 1; //VirtueMartModelVendor::getLoggedVendor();
			$db = JFactory::getDBO();
			$q = 'SELECT   `vendor_currency` FROM `#__virtuemart_vendors` WHERE `virtuemart_vendor_id`=' . $vendorId;
			$db->setQuery($q);
			$emailCurrencyId = $db->loadResult();
		} else {
			$emailCurrencyId = $payments[0]->email_currency;
		}

	}

	/**
	 * @param $html
	 * @return bool|null|string
	 */
	function plgVmOnPaymentResponseReceived(&$html) {


		if (!class_exists('VirtueMartCart')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'cart.php');
		}
		if (!class_exists('shopFunctionsF')) {
			require(VMPATH_SITE . DS . 'helpers' . DS . 'shopfunctionsf.php');
		}
		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}
		VmConfig::loadJLang('com_virtuemart_orders', TRUE);

                $cashfree_response = array();	
                $cashfree_response["orderId"] = vRequest::getString('orderId', "");
                $cashfree_response["orderAmount"] = vRequest::getString('orderAmount', "");
		$cashfree_response["txStatus"] = vRequest::getString('txStatus', "");
		$cashfree_response["referenceId"] = vRequest::getString('referenceId', "");
		$cashfree_response["txTime"] = vRequest::getString('txTime', "");
		$cashfree_response["txMsg"] = vRequest::getString('txMsg', "");
		$cashfree_response["paymentMode"] = vRequest::getString('paymentMode', "");
		$cashfree_response["signature"] = vRequest::getString('signature', "");
 

		// the payment itself should send the parameter needed.
		$virtuemart_paymentmethod_id = vRequest::getInt('pm', 0);
		
		$order_number = vRequest::getString('on', 0);


		$vendorId = 0;

		if (!($this->_currentMethod = $this->getVmPluginMethod($virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}

		if (!$this->selectedThisElement($this->_currentMethod->payment_element)) {
			return NULL;
		}

		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return NULL;
		}

		if (!($payments = $this->getDatasByOrderNumber($order_number))) {
                 $this->redirectToCart("Nothing to process.");
			return NULL;
		}
   
        if ($order_number !=  $cashfree_response["orderId"]) {
        	//Wrong, re-direct
           $this->redirectToCart("No payment to process.");
        }

        //check if this order has already been processed
		$payment_name = $this->renderPluginName($this->_currentMethod);

		VmConfig::loadJLang('com_virtuemart');
	
		// to do: this

        $orderModel = VmModel::getModel('orders');
		$order = $orderModel->getOrder($virtuemart_order_id);

        $response = $this->_processCashFreeResponse($cashfree_response, $payments, $order, $virtuemart_order_id);
   
        if ($response["status"] == 0) {
            $this->redirectToCart($response["message"]);
        }

        $payment = $response["payment"];

        vmdebug('plgVmOnPaymentResponseReceived', $payment);


        if ($cashfree_response["txStatus"] == "FAILED") {
		  $this->redirectToCart("Your payment has failed. Please try again.");
		} else if ($cashfree_response["txStatus"] == "CANCELLED") {
		  $this->redirectToCart("You have cancelled the payment.");
		}

		$html = $this->renderByLayout('response', array("success" => true,
			"payment" => $payment,
			"order" => $order,
			"cashfree_response" => $cashfree_response
		));

		//We delete the old stuff
		// get the correct cart / session
		$cart = VirtueMartCart::getCart();
		$cart->emptyCart();
		return TRUE;
	}

    function plgVmOnPaymentNotification() {

		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}

		$cashfree_response = $_POST;

		if (!isset($cashfree_response['orderId'])) {
			return FALSE;
		}

		$order_number = $cashfree_response['orderId'];
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return FALSE;
		}

		if (!($payments = $this->getDatasByOrderNumber($order_number))) {
			return FALSE;
		}

		$orderModel = VmModel::getModel('orders');
		$order = $orderModel->getOrder($virtuemart_order_id);
       
        $response = $this->_processCashFreeResponse($cashfree_response, $payments, $order, $virtuemart_order_id);
       
        if ($response["status"]) {
        	return TRUE;
        }
        return FALSE;

	 }

	public function _processCashFreeResponse ($cashfree_response, $payments, $order, $virtuemart_order_id) {
       
       if (!($this->_checkSignature($cashfree_response))) {
       	 return array("status" => 0, "message" => "Parameters were invalid");
       }
  
       $payment = end($payments);
       $order_number =  $order['details']['BT']->order_number;

       if ($payment->isProcessed == 1) {
       	  return array("status" => 2, "message" => "Payment has already been processed", "payment" => $payment);
       }

       $order_history = $this->_getOrderStatus($cashfree_response, $payments, $order);
       if (!$order_history || !isset($order_history["order_status"])) {
           return array("status" => 0, "message" => "No payment to process");
	   }

	   $orderModel = VmModel::getModel('orders');
	   
	   $this->_updateCashFreeResponse($cashfree_response, $virtuemart_order_id, $payment->virtuemart_paymentmethod_id, $order_number);
       $orderModel->updateStatusForOneOrder($virtuemart_order_id, $order_history, TRUE);

       if (($order_history["order_status"] == $this->_currentMethod->status_success) && $payment->cashfree_custom) {
         $this->emptyCart($payment->cashfree_custom, $order_number);
       }

       $response = array();
       $response["status"] = 1;
       $response["payment"] = $payment;
       return $response;

	}

	private function _checkSignature($cashfree_response) { 
	    if ($this->_currentMethod->sandbox) {
		   $secret_key = $this->_currentMethod->sandbox_cashfree_secret_key;
		} else {
		   $secret_key = $this->_currentMethod->cashfree_secret_key;
		 }

		$data = "{$cashfree_response['orderId']}{$cashfree_response['orderAmount']}{$cashfree_response['referenceId']}{$cashfree_response['txStatus']}{$cashfree_response['paymentMode']}{$cashfree_response['txMsg']}{$cashfree_response['txTime']}";

        $hash_hmac = hash_hmac('sha256', $data, $secret_key, true) ;
        $computedSignature = base64_encode($hash_hmac);

        if ($cashfree_response["signature"] == $computedSignature) {
         return TRUE;
        } 
        return FALSE;
	}


	private function _getOrderStatus ($cashfree_response, $payments, $order) {			
		$order_history = array();
		$order_history['customer_notified'] = 0;
		$order_history['order_status'] = $this->_currentMethod->status_pending;
        $order_history['comments'] = $cashfree_response["txMsg"];

		if ($cashfree_response['txStatus'] == 'CANCELLED') {
			$order_history['order_status'] = $this->_currentMethod->status_canceled;
		} else {
			if (strcmp($cashfree_response['txStatus'], 'SUCCESS') == 0) {
				$this->debugLog('Completed', 'payment_status', 'debug');
				if ($this->_check_order_id_already_processed($payments, $order, $cashfree_response['orderId'])) {
					$this->debugLog($cashfree_data['orderId'], '_check_order_id_already_processed', 'debug');
					return FALSE;
				}
				$order_history['order_status'] = $this->_currentMethod->status_success;
				$order_history['comments'] = "Success - ".$cashfree_response["txMsg"]." ".$order['details']['BT']->order_number;
			} else {
				$order_history['comments'] = 'No data received';
				$order_history['customer_notified'] = 0;
			}
		}
		return $order_history;
	}


	protected function _check_order_id_already_processed ($payments, $order, $orderId) {
		if ($this->order['details']['BT']->order_status == $this->_method->status_success) {
			foreach ($payments as $payment) {
				$cashfree_data = json_decode($payment->cashfree_fullresponse);
				if ($cashfree_data->orderId == $orderId) {
					return true;
				}
			}
		}
		return false;
	}

	/*********************/
	/* Private functions */
	/*********************/
	private function _loadCashFreeInterface() {
	
	$CashFreeInterface = new CashFreeHelperCashFreeStd($this->_currentMethod, $this);
		
	return $CashFreeInterface;
	}

	private function _updateCashFreeResponse( $cashfree_response, $virtuemart_order_id, $virtuemart_paymentmethod_id, $order_number) {
		// get all know columns of the table
		$db = JFactory::getDBO();
		$query = 'SHOW COLUMNS FROM `' . $this->_tablename . '` ';
		$db->setQuery($query);
		$columns = $db->loadColumn(0);

		
		$response_fields = array();
		if (array_key_exists('txStatus', $cashfree_response)) {
			$response_fields['cashfree_response_payment_status'] = $cashfree_response['txStatus'];
		} 
		 
		if (array_key_exists('txTime', $cashfree_response)) {
		   $response_fields['cashfree_response_payment_date'] = $cashfree_response['txTime'];
		} 	

		if (array_key_exists('txMsg', $cashfree_response)) {
		   $response_fields['cashfree_response_message'] = $cashfree_response['txMsg'];
		}

		if (array_key_exists('referenceId', $cashfree_response)) {
		   $response_fields['cashfree_response_payment_reference_id'] = $cashfree_response['referenceId'];
		}

       if (array_key_exists('paymentMode', $cashfree_response)) {
		   $response_fields['cashfree_response_payment_mode'] = $cashfree_response['paymentMode'];
		}

		if ($cashfree_response) {
			$response_fields['cashfree_fullresponse'] = json_encode($cashfree_response);
		}

        
        $db = JFactory::getDBO();
		$query = 'UPDATE `' . $this->_tablename . '` set isProcessed = 1';
		foreach($response_fields as $key => $value) {
			$query .= ", $key = '$value'";
			$i++;
		}
        $query .= " where virtuemart_order_id = ". $virtuemart_order_id." and order_number = '". $order_number."' and virtuemart_paymentmethod_id = ".$virtuemart_paymentmethod_id;
        $db->setQuery($query);
        $db->loadResult();

	}

	/**
	 * @param   int $virtuemart_order_id
	 * @param string $order_number
	 * @return mixed|string
	 */
	private function _getCashFreeInternalData($virtuemart_order_id, $order_number = '') {
		if (empty($order_number)) {
			$orderModel = VmModel::getModel('orders');
			$order_number = $orderModel->getOrderNumber($virtuemart_order_id);
		}
		$db = JFactory::getDBO();
		$q = 'SELECT * FROM `' . $this->_tablename . '` WHERE ';
		$q .= " `order_number` = '" . $order_number . "'";

		$db->setQuery($q);
		if (!($payments = $db->loadObjectList())) {
			// JError::raiseWarning(500, $db->getErrorMsg());
			return '';
		}
		return $payments;
	}



	//Order has been cancelled. If it was a succesful payment, then trigger a request for refund

	function plgVmOnUserPaymentCancel() {

		if (!class_exists('VirtueMartModelOrders')) {
			require(VMPATH_ADMIN . DS . 'models' . DS . 'orders.php');
		}

		$order_number = vRequest::getString('on', '');
		$virtuemart_paymentmethod_id = vRequest::getInt('pm', '');
		if (empty($order_number) or empty($virtuemart_paymentmethod_id) or !$this->selectedThisByMethodId($virtuemart_paymentmethod_id)) {
			return NULL;
		}
		if (!($virtuemart_order_id = VirtueMartModelOrders::getOrderIdByOrderNumber($order_number))) {
			return NULL;
		}
		if (!($paymentTable = $this->getDataByOrderNumber($order_number))) {
			return NULL;
		}

		VmInfo(vmText::_('VMPAYMENT_CashFree_PAYMENT_CANCELLED'));
		$session = JFactory::getSession();
		$return_context = $session->getId();
		if (strcmp($paymentTable->cashFree_custom, $return_context) === 0) {
			//$this->handlePaymentUserCancel($virtuemart_order_id);
		}
		return TRUE;
	}

	protected function renderPluginName($activeMethod) {
		$return = '';
		$plugin_name = $this->_psType . '_name';
		$plugin_desc = $this->_psType . '_desc';
		$description = '';
		// 		$params = new JParameter($plugin->$plugin_params);
		// 		$logo = $params->get($this->_psType . '_logos');
		$logosFieldName = $this->_psType . '_logos';
		$logos = $activeMethod->$logosFieldName;
		if (!empty($logos)) {
			$return = $this->displayLogos($logos) . ' ';
		}
		$pluginName = $return . '<span class="' . $this->_type . '_name">' . $activeMethod->$plugin_name . '</span>';
		
		if (!empty($activeMethod->$plugin_desc)) {
			$pluginName .= '<span class="' . $this->_type . '_description">' . $activeMethod->$plugin_desc . '</span>';
		}
		if ($activeMethod->sandbox) {
			$pluginName .= ' <small style="color:red;font-weight:bold">Sandbox</small>';
		}
		return $pluginName;
	}


	/**
	 * Display stored payment data for an order
	 *
	 * @see components/com_virtuemart/helpers/vmPSPlugin::plgVmOnShowOrderBEPayment()
	 */
	function plgVmOnShowOrderBEPayment($virtuemart_order_id, $payment_method_id) {

		if (!$this->selectedThisByMethodId($payment_method_id)) {
			return NULL; // Another method was selected, do nothing
		}
		if (!($this->_currentMethod = $this->getVmPluginMethod($payment_method_id))) {
			return FALSE;
		}
		if (!($payments = $this->_getCashFreeInternalData($virtuemart_order_id))) {
			// JError::raiseWarning(500, $db->getErrorMsg());
			return '';
		}

		//$html = $this->renderByLayout('orderbepayment', array($payments, $this->_psType));
		$html = '<table class="adminlist table" >' . "\n";
		$html .= $this->getHtmlHeaderBE();
		$code = "CashFree_response_";
		$first = TRUE;
		foreach ($payments as $payment) {
			$html .= ' <tr><td><strong>Transaction Time</strong></td><td align="left"><strong>' . $payment->cashfree_response_payment_date . '</strong></td></tr> ';
            $html .= ' <tr><td><strong>Transaction Status</strong></td><td align="left"><strong>' . $payment->cashfree_response_payment_status . '</strong></td></tr> ';
          
            if ($payment->cashfree_response_payment_status == "SUCCESS") {
			  $html .= ' <tr><td><strong>Payment Mode</strong></td><td align="left"><strong>' . $payment->cashfree_response_payment_mode . '</strong></td></tr> ';
			  $html .= ' <tr><td><strong>CashFree Reference Id</strong></td><td align="left"><strong>' . $payment->cashfree_response_payment_reference_id . '</strong></td></tr> ';
            }
			// Now only the first entry has this data when creating the order
			if ($first) {
				$html .= $this->getHtmlRowBE('COM_VIRTUEMART_PAYMENT_NAME', $payment->payment_name);
				// keep that test to have it backwards compatible. Old version was deleting that column  when receiving an IPN notification
				if ($payment->payment_order_total and  $payment->payment_order_total != 0.00) {
					$html .= $this->getHtmlRowBE('COM_VIRTUEMART_TOTAL', $payment->payment_order_total . " " . shopFunctions::getCurrencyByID($payment->payment_currency, 'currency_code_3'));
				}

				$first = FALSE;
			} else {
				$CashFreeInterface = $this->_loadCashFreeInterface();

				if (isset($payment->cashFree_fullresponse) and !empty($payment->cashFree_fullresponse)) {
					$cashFree_data = json_decode($payment->cashFree_fullresponse);
					$CashFreeInterface = $this->_loadCashFreeInterface();
					$html .= $CashFreeInterface->onShowOrderBEPayment($cashFree_data);

					$html .= '<tr><td></td><td>
    <a href="#" class="CashFreeLogOpener" rel="' . $payment->id . '" >
        <div style="background-color: white; z-index: 100; right:0; display: none; border:solid 2px; padding:10px;" class="vm-absolute" id="CashFreeLog_' . $payment->id . '">';

					foreach ($CashFree_data as $key => $value) {
						$html .= ' <b>' . $key . '</b>:&nbsp;' . $value . '<br />';
					}

					$html .= ' </div>
        <span class="icon-nofloat vmicon vmicon-16-xml"></span>&nbsp;';
					$html .= "Transaction Log";
					$html .= '  </a>';
					$html .= ' </td></tr>';
				} else {
					$html .= $CashFreeInterface->onShowOrderBEPaymentByFields($payment);
				}
			}


		}
		$html .= '</table>' . "\n";


		return $html;

	}




	/**
	 * @param $jplugin_id
	 * @return bool|mixed
	 */
	function plgVmOnStoreInstallPaymentPluginTable($jplugin_id) {
		if ($jplugin_id != $this->_jid) {
			return FALSE;
		}
	
	    $sandbox = "";
	    $sandbox_param = "";
		if ($this->_currentMethod->sandbox) {
		  $sandbox = 'SANDBOX ';
		  $sandbox_param = 'sandbox_';
		}


		$param = $sandbox_param . 'cashfree_app_id';
		if (empty ($this->_currentMethod->$param)) {
		    $text = "Please provide {$sandbox}app Id";
		    vmWarn($text);
		}
	
		$param = $sandbox_param . 'cashfree_secret_key';
		if (empty ($this->_currentMethod->$param)) {
		    $text = "Please provide {$sandbox}secret key";
			vmWarn($text);
		}

		return $this->onStoreInstallPluginTable($jplugin_id);
	}

	/**
	 *     * This event is fired after the payment method has been selected.
	 * It can be used to store additional payment info in the cart.
	 * @param VirtueMartCart $cart
	 * @param $msg
	 * @return bool|null
	 */
	public function plgVmOnSelectCheckPayment(VirtueMartCart $cart, &$msg) {

		if (!$this->selectedThisByMethodId($cart->virtuemart_paymentmethod_id)) {
			return null; // Another method was selected, do nothing
		}

		if (!($this->_currentMethod = $this->getVmPluginMethod($cart->virtuemart_paymentmethod_id))) {
			return FALSE;
		}

		$CashFreeInterface = $this->_loadCashFreeInterface();
		$CashFreeInterface->setCart($cart);
		$CashFreeInterface->setTotal($cart->cartPrices['billTotal']);
		$CashFreeInterface->loadCustomerData();

		if ($cart->cartPrices['billTotal'] < 0)	{
			VmInfo('This mode is not valid for current cart amount');
			return false;
		}	


		return true;
	}

	/*******************/
	/* Order cancelled */
	/* May be it is removed in VM 2.1
	/*******************/
	public function plgVmOnCancelPayment(&$order, $old_order_status) {
		return NULL;

	}

	/**
	 *  Order status changed
	 * @param $order
	 * @param $old_order_status
	 * @return bool|null
	 */
	public function plgVmOnUpdateOrderPayment(&$order, $old_order_status) {


		//Load the method
		if (!($this->_currentMethod = $this->getVmPluginMethod($order->virtuemart_paymentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}

		if (!$this->selectedThisElement($this->_currentMethod ->payment_element)) {
			return NULL;
		}

         if (!$this->_currentMethod->order_change_trigger) {
         	return NULL;
         }

		//Check for cancel and refund
		if ($order->order_status != $this->_currentMethod->status_canceled || $order->order_status != $this->_currentMethod->status_refunded) {
			return NULL;
		}

		//Load the payments
		if (!($payments = $this->_getCashFreeInternalData($order->virtuemart_order_id))) {
			// JError::raiseWarning(500, $db->getErrorMsg());
			return NULL;
		}

		$payment = end($payments);
		if ($payment->cashfree_response_payment_status == 'SUCCESS') {
		   $cashfree_data = json_decode($payment->cashfree_fullresponse);
            $orderAmount = $cashfree_data->orderAmount;
            $referenceId = $payment->cashfree_response_payment_reference_id;
			//Trigger the refund API here
			 $CashFreeInterface = $this->_loadCashFreeInterface();
			 if($CashFreeInterface->triggeRefund($payment)) {
			 	//update payment status to REFUNDED
			 }
		} 
		return true;
	}

	function plgVmOnUpdateOrderLinePayment(&$order) {
		// $xx=1;
	}



	/**
	 * * List payment methods selection
	 * @param VirtueMartCart $cart
	 * @param int $selected
	 * @param $htmlIn
	 * @return bool
	 */

	public function plgVmDisplayListFEPayment(VirtueMartCart $cart, $selected = 0, &$htmlIn) {
		
      return $this->displayListFE ($cart, $selected, $htmlIn);
    }


	/**
	 * Validate payment on checkout
	 * @param VirtueMartCart $cart
	 * @return bool|null
	 */
	function plgVmOnCheckoutCheckDataPayment(VirtueMartCart $cart) {

		if (!$this->selectedThisByMethodId($cart->virtuemart_paymentmethod_id)) {
			return NULL; // Another method was selected, do nothing
		}

		if (!($this->_currentMethod = $this->getVmPluginMethod($cart->virtuemart_paymentmethod_id))) {
			return FALSE;
		}

		//If CashFree express, make sure we have a valid token.
		//If not, redirect to CashFree to get one.
		$CashFreeInterface = $this->_loadCashFreeInterface();

		$CashFreeInterface->setCart($cart);
		$cart->getCartPrices();
		$CashFreeInterface->setTotal($cart->cartPrices['billTotal']);

		// Here we only check for token, but should check for payer id ?
		$CashFreeInterface->loadCustomerData();
		$CashFreeInterface->getExtraPluginInfo($this->_currentMethod);
		$expressCheckout = vRequest::getVar('expresscheckout', '');
		if ($expressCheckout == 'cancel') {
			return true;
		}
		if (!$CashFreeInterface->validate()) {
			return false;
		}

		return true;
		//Validate amount
		//if ($totalInPaymentCurrency <= 0) {
		//	vmInfo (vmText::_ ('VMPAYMENT_CashFree_PAYMENT_AMOUNT_INCORRECT'));
		//	return FALSE;
		//}
	}


	//Calculate the price (value, tax_id) of the selected method, It is called by the calculator
	//This function does NOT to be reimplemented. If not reimplemented, then the default values from this function are taken.
	public function plgVmOnSelectedCalculatePricePayment(VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
		if (!($selectedMethod = $this->getVmPluginMethod($cart->virtuemart_paymentmethod_id))) {
			return FALSE;
		}
		//$this->isExpToken($selectedMethod, $cart) ;
		return $this->onSelectedCalculatePrice($cart, $cart_prices, $cart_prices_name);
	}




	// Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
	// The plugin must check first if it is the correct type
	function plgVmOnCheckAutomaticSelectedPayment(VirtueMartCart $cart, array $cart_prices = array(), &$paymentCounter) {
		return $this->onCheckAutomaticSelected($cart, $cart_prices, $paymentCounter);
	}

	// This method is fired when showing the order details in the frontend.
	// It displays the method-specific data.
	public function plgVmOnShowOrderFEPayment($virtuemart_order_id, $virtuemart_paymentmethod_id, &$payment_name) {
		$this->onShowOrderFE($virtuemart_order_id, $virtuemart_paymentmethod_id, $payment_name);
	}

	// This method is fired when showing when priting an Order
	// It displays the the payment method-specific data.
	function plgVmonShowOrderPrintPayment($order_number, $method_id) {
		return $this->onShowOrderPrint($order_number, $method_id);
	}

	function plgVmDeclarePluginParamsPaymentVM3( &$data) {
		return $this->declarePluginParams('payment', $data);
	}

	function plgVmSetOnTablePluginParamsPayment($name, $id, &$table) {
		return $this->setOnTablePluginParams($name, $id, $table);
	}

}

// No closing tag
